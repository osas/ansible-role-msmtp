Ansible module used by OSAS to deploy simple SMTP relay

The role by itself is to be used in complement of a full featured SMTP smarthost
somewhere in the infrastructure. Such role is commenly done by postfix, but there is no
strict requirements on that.

By default, the role will relay all mail to the smtp smarthost, who must be configured to either
receive the mail for that domain, or a custom auth must be set (which is not supported
yet).

The role will also by default integrate with a IPA installation and verify SSL certificates 
against the CA of the IPA setup, unless this is disabled with `disable_freeipa` parameter. 

The role will also not deploy itself on the smarthost, making it safe to be deployed
by default on the whole infrastructure.

# Example

```
$ cat deploy_msmtp.yml
- hosts: all
  roles:
  - role: msmtp
    smart_host: mail.example.org
```
